<!DOCTYPE html>
<html lang="en">
    <head>
        <title><?php echo e($title); ?></title>
        <meta name="author" content="Logan McCourry">
        <meta name="Description" content="Resume Site built in Laravel 5 for Logan McCourry">
        <meta name="keyword" content="resume, personal, logan, mccourry, loganmccourry">
        <link href="<?php echo e(asset('semantic-ui/dist/semantic.min.css')); ?>" rel="stylesheet" type="text/css">
        <link href="<?php echo e(asset('css/main.css')); ?>" rel="stylesheet" type="text/css">
        <script src="<?php echo e(asset('jquery/dist/jquery.min.js')); ?>" rel="script" type="text/javascript"></script>
        <script src="<?php echo e(asset('semantic-ui/dist/semantic.js')); ?>" rel="script" type="text/javascript"></script>
        <?php echo $__env->yieldContent('head'); ?>
    </head>
    <body>
        <div id="mainContainer" class="ui two column grid container">
            <div class="four wide column">
                <h1 class="ui header">Logan Mccourry</h1>
                <p>Full Stack Developer</p>
            </div>
            <div class="ten wide column">
                <h2 class="ui header">Career Profile</h2>
                <p>Full Stack Web Developer</p>
            </div>
        </div>
    </body>
</html>